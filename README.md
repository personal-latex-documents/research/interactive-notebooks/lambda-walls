Link to latest HTML renders on main branch of gitlab repo:

## Demos

|Features | SageMath Notebook | Python+Sympy Notebook|
|---------|-------------------|----------------------|
| Chern Characters |[html](https://git.ecdf.ed.ac.uk/personal-latex-documents/research/interactive-notebooks/lambda-walls/-/jobs/artifacts/main/raw/build/demo.sage.html?job=sage-build) | [html](https://git.ecdf.ed.ac.uk/personal-latex-documents/research/interactive-notebooks/lambda-walls/-/jobs/artifacts/main/raw/build/demo.sympy.html?job=sympy-build) |
| Stability Conditions | [html](https://git.ecdf.ed.ac.uk/personal-latex-documents/research/interactive-notebooks/lambda-walls/-/jobs/artifacts/main/raw/build/stability.sage.html?job=sage-build) | [html](https://git.ecdf.ed.ac.uk/personal-latex-documents/research/interactive-notebooks/lambda-walls/-/jobs/artifacts/main/raw/build/stability.sympy.html?job=sympy-build)| 

## Other

[exploration](https://git.ecdf.ed.ac.uk/personal-latex-documents/research/interactive-notebooks/lambda-walls/-/jobs/artifacts/main/raw/build/exploration.html?job=sage-build)

[Ideal Sheaf of Line Example](https://git.ecdf.ed.ac.uk/personal-latex-documents/research/interactive-notebooks/lambda-walls/-/jobs/artifacts/Ideal_Sheaf_Example/raw/build/Ideal_sheaf_example.html?job=sage-build)

# Try it Yourself

With Python or SageMath installed you can install the code here as a [pip package](https://git.ecdf.ed.ac.uk/personal-latex-documents/research/interactive-notebooks/lambda-walls/-/packages/):

```bash
pip install pseudowalls --extra-index-url https://git.ecdf.ed.ac.uk/api/v4/projects/9689/packages/pypi/simple
```

```bash
sage -pip install pseudowalls --extra-index-url https://git.ecdf.ed.ac.uk/api/v4/projects/9689/packages/pypi/simple
```

The demo `.ipynb` files can be opened in Jupyter notebook (to be installed, should be included with SageMath).
