import setuptools

setuptools.setup(
    name = "pseudowalls",
    version = "0.0.2",
    author = "Luke Naylor",
    author_email = "luke.naylr@gmail.com",
    description = "Utilities for computations related to stability condition walls",
    packages = setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    requires='sympy'
)

