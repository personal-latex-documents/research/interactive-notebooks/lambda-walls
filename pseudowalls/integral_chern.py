from sympy.core.add import Add as sympy_add
from .chern_character import Chern_Char
from .utils.symb_util import latex, symbols, factorial 

class Integral_Chern(Chern_Char):

    def __init__(self,*ch):
        adjusted = (chi/factorial(i) for i, chi in enumerate(ch))
        Chern_Char.__init__(self, *adjusted)

    def _parametrization(self):
        def brackets_needed(exponent, expression):
            if hasattr(expression, "_sympy_"):
                # Tweak for Sage expressions
                expression = expression._sympy_()
            return exponent > 0 \
              and hasattr(expression, "func") \
              and expression.func is sympy_add

        return (
          r"\begin{array}{l} "
          + r" \\ ".join(
            # ch_i:
            r"\mathrm{ch}_{"+latex(i)+r"} ="
            + (r"\left(" if brackets_needed(i, vi) else "")
            + latex(vi*factorial(i))
            + (r"\right)" if brackets_needed(i, vi) else "")
            # add l^i/i! when i>0:
            + (
                r"\frac{"
                + f"\\ell^{{{i}}}"
                + r"}{" + str(i) + "!}"
                if i > 0 else ""
              )
            for i, vi in enumerate(self.ch)
          )
          + r" \end{array}"
        )
    
#############################
## Alternative Constructors #
#############################

def generic_integral_chern(g, name = "a"):
    ch = symbols(
        " ".join(
            name+f"_{i}"
            for i in range(g+1)
        ),
        integer = True
    )

    return Integral_Chern(*ch)
