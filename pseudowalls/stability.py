from .utils.stability_util import stab_from_central_charge, stab_from_rank_degree
from .chern_character import Chern_Char
from .utils.symb_util import I, Symbol, Rational

@stab_from_rank_degree
class Mumford:

    def rank(self, v):
        return v.ch[0]

    def degree(self, v):
        return v.ch[1]


@stab_from_central_charge
class Tilt:

    def __init__(
      self,
      alpha = Symbol("alpha", real = True, positive = True),
      beta = Symbol("beta", real = True)
    ):
        self.alpha = alpha
        self.beta = beta
    
    def central_charge(self, v):
        return - v.twist(self.beta + I*self.alpha).ch[2]

@stab_from_rank_degree
class SecondTilt:

    def __init__(
      self,
      alpha = Symbol("alpha", real = True, positive = True),
      beta = Symbol("beta", real = True),
      s = Symbol("s", real = True)
    ):
        self.alpha = alpha
        self.beta = beta
        self.s = s

    def rank(self, v):
        return Tilt(self.alpha, self.beta).degree(v)

    def degree(self, v):
        _, ch1, _, ch3, *_ = v.twist(self.beta).ch
        SIXTH = Rational(1,6)

        return ch3 - (self.s + SIXTH) * self.alpha**2 * ch1

# Alternative aliases for these classes

Lambda = SecondTilt
nu = Tilt
