from .utils.symb_util import symbols, latex, factorial

from sympy.core.add import Add as sympy_add

##########################
## Chern Character type ##
##########################

class Chern_Char:
    """
    Class for representing Chern Characters
    of a variety X of Picard Rank 1
    """
    # This objects of this class definitely must be
    # considered immutable from the outside
    # TODO find out if this can be enforced

    def __init__(self, *ch):
        r"""
        Given a generator $L \in NS(X)$ with $\dim(X)=g$,
        The initialiser takes $g+1$ elements, where the ith 
        element $ch_{i-1}$, the coefficient of $\[L\]^{i-1}$
        in $\mathrm{chern}_{i-1} = ch_i \[L\]^{i-1}$
        """
        self.ch = ch

    def _title(self):
        return r"Chern Character:"
    
    def _parametrization(self):
        def brackets_needed(exponent, expression):
            if hasattr(expression, "_sympy_"):
                # Tweak for Sage expressions
                expression = expression._sympy_()
            return exponent > 0 \
              and hasattr(expression, "func") \
              and expression.func is sympy_add

        return (
          r"\begin{array}{l} "
          + r" \\ ".join(
            # ch_i:
            r"\mathrm{ch}_{"+str(i)+r"} ="
            + (r"\left(" if brackets_needed(i,vi) else "")
            + latex(vi)
            + (r"\right)" if brackets_needed(i,vi) else "")
            + (r"\ell^{"+str(i)+"}" if i>0 else "")
            for i, vi in enumerate(self.ch)
          )
          + r" \end{array}"
        )

    def _repr_latex_(self):
        return (
          self._title()
          + self._parametrization()
        )

    def _latex_(self):
        return (
          r"\text{"
          + self._title()
          + r"} \\ "
          + self._parametrization()
        )

    def __mul__(self, other):
        assert len(self.ch) == len(other.ch), f"""
        Chern characters can only be multiplied if they have
        the same length:
            LHS length = {len(self.ch)}
            RHS length = {len(other.ch)}
        """

        return Chern_Char(*(
            # ch_i of the product:
            sum(
                self.ch[j]*other.ch[i-j]
                for j in range(i+1)
            )
            for i in range(len(self.ch))
        ))
        
    def __neg__(self):
        return Chern_Char(*( - ch for ch in self.ch))
        
    def __add__(self, other):
        assert len(self.ch) == len(other.ch), f"""
        Chern characters can only be added if they have
        the same length:
            LHS length = {len(self.ch)}
            RHS length = {len(other.ch)}
        """

        return Chern_Char(*(
            # ch_i of the sum:
            a + b
            for a, b in zip(self.ch, other.ch)
        ))

    def __sub__(self, other):
        assert len(self.ch) == len(other.ch), f"""
        Chern characters can only be subtracted if they have
        the same length:
            LHS length = {len(self.ch)}
            RHS length = {len(other.ch)}
        """

        return Chern_Char(*(
            # ch_i of the sum:
            a - b
            for a, b in zip(self.ch, other.ch)
        ))

    def twist(self, beta):
        g = len(self.ch) - 1
        twisted  = exponential_chern(-beta, g) * self

        result = Twisted_Chern_Char(beta, *twisted.ch)

        # twist inherits the chosen "parametrisation"
        # i.e. the way it's written out
        # Behold the world's most despicable hack:
        result._parametrization = \
          lambda : self.__class__._parametrization(result)

        return result

    def Q_tilt(self):
        v0, v1, v2, *_ = self.ch
        return v1**2 - 2*v0*v2


##################################
## Twisted Chern Character type ##
##################################

class Twisted_Chern_Char(Chern_Char):
    def __init__(self, beta, *ch):
        self.ch = ch
        self.beta = beta
    
    def _title(self):
        return (
          r"Twisted Chern Character for "
          + r"$\beta={"
          + latex(self.beta)
          + r"}$"
        )

    def twist(self, beta):
        g = len(self.ch) - 1
        twisted  = exponential_chern(self.beta-beta, g) * self

        return Twisted_Chern_Char(beta, *twisted.ch)


#############################
## Alternative Constructors #
#############################

def exponential_chern(x, g):
    return Chern_Char(*(
        x**i / factorial(i)
        for i in range(g+1)
    ))

def generic_chern_char(g, name = "ch"):
    ch = symbols(
        " ".join(
            name+f"_{i}"
            for i in range(g+1)
        ),
        rational = True
    )

    return Chern_Char(*ch)
